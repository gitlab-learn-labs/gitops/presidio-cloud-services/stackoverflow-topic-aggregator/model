import json


def writes_json(dictionary):
    """creates json file based on the input dictionary extracted from API Call"""
    with open('questions/dataset-questions.json', 'w') as outfile:
        json.dump(dictionary, outfile, indent=2)

