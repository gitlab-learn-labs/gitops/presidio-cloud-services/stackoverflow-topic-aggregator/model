from sqlalchemy import create_engine
import argparse
import os
#engine = create_engine('postgresql+psycopg2://'+os.environ['USER']+':'+os.environ['PASSWORD']+'@localhost:3306/')


def get_args():
    """Gets arguments passed using cmd line in CI"""
    parser = argparse.ArgumentParser()
    parser.add_argument('--credential_file', type=argparse.FileType('r'))
    parser.add_argument('--instances', type=str)

    return parser.parse_args()